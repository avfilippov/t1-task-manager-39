package ru.t1.avfilippov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.Project;

import java.util.List;

public interface IProjectRepository  {

    @Insert("INSERT INTO tm.tm_project (id,created,name,description,status,user_id)" +
            " VALUES (#{id},#{created},#{name},#{description},#{status},#{userId})")
    void add (@NotNull Project project);

    @Delete("DELETE FROM tm.tm_project WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Project> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm.tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Project findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm.tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Project findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm.tm_project WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull Project project);

    @Delete("DELETE FROM tm.tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm.tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    void removeByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Update("UPDATE tm.tm_project SET name = #{name}, created = #{created}, description = #{description}," +
            " status = #{status}, user_id = #{userId} WHERE id = #{id}")
    void update(@NotNull Project project);

    @Select("SELECT COUNT(*) from tm.tm_project WHERE user_id = #{userId}")
    long getSize(@NotNull @Param("userId") String userId);

}
