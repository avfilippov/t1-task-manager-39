package ru.t1.avfilippov.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class TestClientData {

    @NotNull
    public static final String USER_TEST_LOGIN = "qwerty1";

    @NotNull
    public static final String USER_TEST_PASS = "qwerty1";

    @NotNull
    public static final String USER2_TEST_LOGIN = "user1";

    @NotNull
    public static final String USER2_TEST_PASS = "user1";

    @NotNull
    public static final String ADMIN_TEST_LOGIN = "admin";

    @NotNull
    public static final String ADMIN_TEST_PASS = "admin";

}
