package ru.t1.avfilippov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.endpoint.*;
import ru.t1.avfilippov.tm.api.service.*;
import ru.t1.avfilippov.tm.endpoint.*;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.model.User;
import ru.t1.avfilippov.tm.service.*;
import ru.t1.avfilippov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService , sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

        @NotNull
        private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

        @NotNull
        private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

        @NotNull
        private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

        @NotNull
        private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    {
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(systemEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }


    public void start() {
        initPID();
       // initDemoData();
        loggerService.info("*** TASK MANAGER SERVER STARTED ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        loggerService.info("*** TASK MANAGER SERVER IS SHUTTING DOWN***");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("TEST", "TEST", "test@test.ru");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST1", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("TEST2", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("TEST3", Status.IN_PROGRESS));

        taskService.add(test.getId(), new Task("TEST1", Status.NOT_STARTED));
        taskService.add(admin.getId(), new Task("TEST2", Status.NOT_STARTED));
    }

}
