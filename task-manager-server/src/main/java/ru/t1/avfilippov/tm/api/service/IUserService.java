package ru.t1.avfilippov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User findByLogin(@Nullable String login);

    @NotNull
    User findByEmail(@Nullable String email);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @Nullable
    User removeOne(@Nullable User model);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    @Nullable
    @SneakyThrows
    User findOneById(@Nullable String id);

    @Nullable
    @SneakyThrows
    List<User> findAll();

    @NotNull
    @SneakyThrows
    Collection<User> set(@NotNull Collection<User> models);

}
