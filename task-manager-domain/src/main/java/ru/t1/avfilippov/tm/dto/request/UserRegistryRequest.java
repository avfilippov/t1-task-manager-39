package ru.t1.avfilippov.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistryRequest extends AbstractUserRequest {

    public UserRegistryRequest(@NotNull final String token) {
        super(token);
    }

    @Nullable private String login;

    @Nullable private String password;

    @Nullable private String email;

}
