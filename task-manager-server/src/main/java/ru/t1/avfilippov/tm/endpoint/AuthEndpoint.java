package ru.t1.avfilippov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.avfilippov.tm.api.service.IAuthService;
import ru.t1.avfilippov.tm.api.service.IServiceLocator;
import ru.t1.avfilippov.tm.api.service.IUserService;
import ru.t1.avfilippov.tm.dto.request.UserLoginRequest;
import ru.t1.avfilippov.tm.dto.request.UserLogoutRequest;
import ru.t1.avfilippov.tm.dto.request.UserProfileRequest;
import ru.t1.avfilippov.tm.dto.response.UserLoginResponse;
import ru.t1.avfilippov.tm.dto.response.UserLogoutResponse;
import ru.t1.avfilippov.tm.dto.response.UserProfileResponse;
import ru.t1.avfilippov.tm.model.Session;
import ru.t1.avfilippov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    @NotNull
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @Override
    @NotNull
    @WebMethod
    public  UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final Session session = check(request);
        getServiceLocator().getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

}
